---
layout: report
year: "2019"
month: "08"
month_name: "August"
title: "Reproducible builds in August 2019"
draft: true
---

## Software development

#### Upstream patches

The Reproducible Builds project detects, dissects and attempts to fix as many currently-unreproducible packages as possible. We endeavour to send all of our patches upstream where appropriate. This month, we wrote a large number of such patches, including:

* Bernhard M. Wiedemann:
    * [fwupd](https://bugzilla.opensuse.org/show_bug.cgi?id=1143905) (report hash over unreproducible LTO data)
    * [kernel-vanilla](https://lists.opensuse.org/opensuse-kernel/2019-08/msg00000.html) (drop number of CPUs)
    * [kernel-obs-build](https://lists.opensuse.org/opensuse-kernel/2019-08/msg00001.html) (date from /etc/shadow)
    * [dracut](https://github.com/dracutdevs/dracut/issues/617) (report CPU influencing build result)
    * [katacontainers-image-initrd/osbuilder](https://github.com/kata-containers/osbuilder/pull/340) (merged ; shell date - new variant with nanoseconds)
    * [buildad](https://github.com/containers/buildah/pull/1805) (date)
    * [nethack](https://build.opensuse.org/request/show/722212) (date (not submitted upstream); tar)
    * [python-python3-saml](https://github.com/onelogin/python3-saml/pull/156) (fix FTBFS-2020 - more issues further in the future)
    * [gnutls](https://gitlab.com/gnutls/gnutls/merge_requests/1058) (date / copyright year)
    * [pcc](https://bugzilla.opensuse.org/show_bug.cgi?id=1146634) (report unreproducibility when building with Link Time Optimization)
    * [libfaketime](https://github.com/wolfcw/libfaketime/issues/183) (toolchain: fix various builds under libfaketime)
* Chris Lamb:
    * [desktop-file-utils](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=872728) (sort file list)
    * [libchamplain](https://gitlab.gnome.org/GNOME/libchamplain/merge_requests/9) (path)
    * [re2c](https://github.com/skvadrik/re2c/pull/258) (shell date)

#### diffoscope

* Vagrant Cascadian updated to [diffoscope 120 in GNU Guix](https://git.savannah.gnu.org/cgit/guix.git/commit/?id=c91364d36cf6c8fc4c696d151eb9fca7832cf898)

#### libfaketime
A [fix to libfaketime](https://github.com/wolfcw/libfaketime/issues/183) can now help better debug or fix reproducibility issues.

## FIXME

* [FIXME](https://github.com/molior-dbs/molior/issues/3) - molior: use "repeatable build" instead of "reproducible build"

* The first Debian package sets have become 100% reproducible: [Debian essential package set for bullseye/amd64](https://tests.reproducible-builds.org/debian/bullseye/amd64/pkg_set_essential.html) and [armhf](https://tests.reproducible-builds.org/debian/bullseye/armhf/pkg_set_essential.html). perl still has isses on [i386](https://tests.reproducible-builds.org/debian/rb-pkg/bullseye/i386/diffoscope-results/perl.html) and [arm64](https://tests.reproducible-builds.org/debian/rb-pkg/bullseye/arm64/diffoscope-results/perl.html)

* [FIXME](https://bugs.debian.org/934511)

* [FIXME](https://bugs.debian.org/934405) - a report on how Debian binary non-maintainer uploads influence `SOURCE_DATE_EPOCH`

* [FIXME](https://github.com/redhotpenguin/perl-Archive-Zip/issues/26#issuecomment-521408412) - perl-Archive-Zip still has broken bzip handling - [affects strip-nondeterminism](https://salsa.debian.org/reproducible-builds/strip-nondeterminism/issues/1)

* [Hardening the "file" utility for Debian -- nb. paywalled for now but will not be by report publication date](https://lwn.net/Articles/796108)

* [FIXME](https://github.com/skvadrik/re2c/pull/258)

* [FIXME](https://github.com/rubygems/rubygems/issues/2290#issuecomment-522206365)

* [FIXME](https://tests.reproducible-builds.org/debian/issues/unstable/captures_varying_number_of_build_path_directory_components_issue.html)

* [FIXME](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=935127)

* [FIXME](https://www.zdnet.com/article/backdoor-found-in-webmin-a-popular-web-based-utility-for-managing-unix-servers/)
